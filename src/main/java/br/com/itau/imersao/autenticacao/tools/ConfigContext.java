package br.com.itau.imersao.autenticacao.tools;

public class ConfigContext {
	public static final String ENV_IMERSAO_URL = System.getenv("IMERSAO-AUTENTICACAO-URL");

	public static String ENDPOINT_VALIDAR_TOKEN(String token) {
		return ENV_IMERSAO_URL + "/usuarios/validar-token/" + token;
	}

	public static String ENDPOINT_IS_USUARIO_REGISTRADO(String username) {
		return ENV_IMERSAO_URL + "/usuarios/registrado/" + username;
	}
	
	public static String ENDPOINT_GET_USUARIO(String username) {
		return ENV_IMERSAO_URL + "usuarios/" + username;
	}
}
