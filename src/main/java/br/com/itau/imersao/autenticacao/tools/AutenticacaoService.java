package br.com.itau.imersao.autenticacao.tools;

import static br.com.itau.imersao.autenticacao.tools.ConfigContext.*;

import org.springframework.web.client.RestTemplate;

public class AutenticacaoService {
	private RestTemplate restTemplate = new RestTemplate();
	
	public String validarToken(String token) {
		return restTemplate.getForObject(ENDPOINT_VALIDAR_TOKEN(token), String.class);
	}
	
	public boolean isUsuarioRegistrado(String username) {
		return restTemplate.getForObject(ENDPOINT_IS_USUARIO_REGISTRADO(username), Boolean.class);
	}
	
	public Usuario getUsuario(String username) {
		return restTemplate.getForObject(ENDPOINT_GET_USUARIO(username), Usuario.class);
	}
	
}
