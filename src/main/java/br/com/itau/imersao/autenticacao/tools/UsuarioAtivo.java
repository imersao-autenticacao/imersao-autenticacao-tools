package br.com.itau.imersao.autenticacao.tools;

public class UsuarioAtivo {

	private Usuario usuario;

	public UsuarioAtivo() {
	}

	public UsuarioAtivo(Usuario usuario) {
		this.usuario = usuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}
