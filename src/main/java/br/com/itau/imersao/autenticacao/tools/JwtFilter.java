package br.com.itau.imersao.autenticacao.tools;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@CrossOrigin
public class JwtFilter implements Filter {
		
	@Override
	public void init(FilterConfig filterConfig) throws ServletException { /* Não utilizado */ }

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		String token = httpRequest.getHeader("Authorization");
		Mensagem mensagem = new Mensagem("Nao autorizado.");
		AutenticacaoService autenticacaoService = new AutenticacaoService();

		if (httpRequest.getMethod().equals("OPTIONS")) {
			chain.doFilter(httpRequest, httpResponse);
			return;
		}
		
		if (token == null) {
			httpResponse = construirResposta(httpResponse, mensagem, HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		token = token.replace("Bearer ", "");
		
		String username = autenticacaoService.validarToken(token);

		if (username == null) {
			httpResponse = construirResposta(httpResponse, mensagem, HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		boolean isUserInDatabase = autenticacaoService.isUsuarioRegistrado(username);

		if (!isUserInDatabase) {
			httpResponse = construirResposta(httpResponse, mensagem, HttpServletResponse.SC_FORBIDDEN);
			return;
		}

		httpResponse.addHeader("Access-Control-Allow-Origin", "*");

		chain.doFilter(httpRequest, httpResponse);
	}

	private HttpServletResponse construirResposta(HttpServletResponse response, Mensagem msg, int sc) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(msg);
			response.setContentType("application/json");
			response.getWriter().write(json);
			response.setStatus(HttpServletResponse.SC_FORBIDDEN);
			return response;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	
	
	

	@Override
	public void destroy() { /* Não utilizado */ }

}
