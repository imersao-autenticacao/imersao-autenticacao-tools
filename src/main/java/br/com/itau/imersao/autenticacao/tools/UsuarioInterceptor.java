package br.com.itau.imersao.autenticacao.tools;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

@Component
public class UsuarioInterceptor extends HandlerInterceptorAdapter {
	private UsuarioAtivo usuarioAtivo;
	
	private RestTemplate restTemplate = new RestTemplate();

	public UsuarioInterceptor(UsuarioAtivo usuarioAtivo) {
		this.usuarioAtivo = usuarioAtivo;
	}

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String authorizationToken = request.getHeader("Authorization");
		AutenticacaoService autenticacaoService = new AutenticacaoService();
		if (authorizationToken != null && !authorizationToken.isEmpty()) {
			authorizationToken = authorizationToken.replace("Bearer ", "");			
			String username = autenticacaoService.validarToken(authorizationToken);
			if (username != null && !username.isEmpty()) {
				Usuario usuarioDb = autenticacaoService.getUsuario(username);
				if (usuarioDb != null) {
					usuarioAtivo.setUsuario(usuarioDb);
				}				
			}
		}
		return true;
	}
}
