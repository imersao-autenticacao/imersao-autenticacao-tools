# Ferramentas para o micro-serviço de usuários

Este projeto foi desenvolvido para ser adicionado nos projetos de micro-serviços, colocando como dependência em seu **pom.xml** da seguinte maneira:

```
<dependency>
    <groupId>br.com.itau.imersao.autenticacao</groupId>
    <artifactId>imersao-autenticacao-tools</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <scope>compile</scope>
</dependency>
```

# Estrutura de arquivos

```
imersao-autenticacao-tools/
├── README.md
├── pom.xml
├── src
│   ├── main
│   │   └── java
│   │       └── br
│   │           └── com
│   │               └── itau
│   │                   └── imersao
│   │                       └── autenticacao
│   │                           └── tools
│   │                               ├── AutenticacaoService.java
│   │                               ├── AutenticacaoTools.java
│   │                               ├── ConfigContext.java
│   │                               ├── JwtFilter.java
│   │                               ├── Mensagem.java
│   │                               ├── Usuario.java
│   │                               ├── UsuarioAtivo.java
│   │                               └── UsuarioInterceptor.java
│   └── test
│       └── java
│           └── br
│               └── com
│                   └── itau
│                       └── imersao
│                           └── autenticacao
│                               └── tools
│                                   └── AppTest.java
└── target
    ├── classes
    │   └── br
    │       └── com
    │           └── itau
    │               └── imersao
    │                   └── autenticacao
    │                       └── tools
    │                           ├── AutenticacaoService.class
    │                           ├── AutenticacaoTools.class
    │                           ├── ConfigContext.class
    │                           ├── JwtFilter.class
    │                           ├── Mensagem.class
    │                           ├── Usuario.class
    │                           ├── UsuarioAtivo.class
    │                           └── UsuarioInterceptor.class
    ├── generated-sources
    │   └── annotations
    ├── generated-test-sources
    │   └── test-annotations
    ├── imersao-autenticacao-tools-0.0.1-SNAPSHOT.jar
    ├── maven-archiver
    │   └── pom.properties
    ├── maven-status
    │   └── maven-compiler-plugin
    │       ├── compile
    │       │   └── default-compile
    │       │       ├── createdFiles.lst
    │       │       └── inputFiles.lst
    │       └── testCompile
    │           └── default-testCompile
    │               ├── createdFiles.lst
    │               └── inputFiles.lst
    ├── surefire-reports
    │   ├── TEST-br.com.itau.imersao.autenticacao.tools.AppTest.xml
    │   └── br.com.itau.imersao.autenticacao.tools.AppTest.txt
    └── test-classes
        └── br
            └── com
                └── itau
                    └── imersao
                        └── autenticacao
                            └── tools
                                └── AppTest.class
```

# Como utilizar em seus micro-serviços


- **Configurar em seu micro-serviço, a variável de ambiente a seguir:**

**Nome:** `IMERSAO-AUTENTICACAO-URL` <br />
**Valor:** URL onde o micro-serviço de usuários está rodando. Ex.: "http://localhost:8090"

- **Para proteger seus endpoints de usuários não autorizados, você deve criar uma classe de configuração que irá registrar o `JwtFilter`.**

Esta classe de configuração preferencialmente deve ser criada em um pacote: **br.com.itau.projeto.config**. O código a ser implementado é o a seguir:

```
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import br.com.itau.imersao.autenticacao.tools.JwtFilter;

@Configuration
public class FilterConfig {
	@Bean
	public FilterRegistrationBean<JwtFilter> checkTokenFilter(){
	    FilterRegistrationBean<JwtFilter> registrationBean = new FilterRegistrationBean<>();
	         
	    registrationBean.setFilter(new JwtFilter());
	    registrationBean.addUrlPatterns("/endpoint-exemplo/*");
	         
	    return registrationBean;    
	}
}
```

Para todas as rotas que desejar ser protegidas pela validação de JWT, adicionar com o método "**addUrlPatterns**".

- **Caso deseje obter dados de usuário que está acessando seu endpoint, para qualquer fim que seja, criar a classe a seguir no mesmo pacote sugerido acima:**

```
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.com.itau.imersao.autenticacao.tools.UsuarioAtivo;
import br.com.itau.imersao.autenticacao.tools.UsuarioInterceptor;

@Configuration
public class MvcConfig implements WebMvcConfigurer {
	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(usuarioInterceptor());
    }

    @Bean(name="usuarioAtivo")
    @Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
    public UsuarioAtivo usuarioAtivo() {    	
        return new UsuarioAtivo();
    }

    @Bean
    public UsuarioInterceptor usuarioInterceptor() {
        return new UsuarioInterceptor(usuarioAtivo());
    }
}
```

Esta classe fará com que seja registrado em seu Spring, um objeto do tipo: **UsuarioAtivo**.
A partir que esta classe de configuração esteja implementada, você poderá acessar o usuário ativo no seu endpoint conforme exemplo a seguir:

```
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.imersao.autenticacao.tools.Usuario;
import br.com.itau.imersao.autenticacao.tools.UsuarioAtivo;
import br.com.itau.rich.produtomicroservice.models.Produto;
import br.com.itau.rich.produtomicroservice.repositories.ProdutoRepository;

@RestController
@RequestMapping("/produto")
public class ProdutoController {

	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private UsuarioAtivo usuarioAtivo;
	
    // Método protegido
	@GetMapping("")    
	public ResponseEntity<Iterable<Produto>> getAll() {
		return ResponseEntity.ok(produtoRepository.findAll());
	}

	// Método protegido
    // Usuário construído utilizando o UsuarioInterceptor
	@GetMapping("/usuario-ativo")
	public ResponseEntity<Usuario> getUsuarioAtivo() {
		return ResponseEntity.ok(usuarioAtivo.getUsuario());
	}
}
```